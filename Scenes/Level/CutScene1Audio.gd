extends AudioStreamPlayer2D

onready var start = preload("res://Assets/Music/night_theme_1_start.wav")
onready var loop = preload("res://Assets/Music/night_theme_1_loop.wav")

var changed = false

# Called when the node enters the scene tree for the first time.
func _ready():
	stream = start
	play()

func _on_AudioStreamPlayer2D_finished():
	if not changed:
		stream = loop
		play()
		changed = true
