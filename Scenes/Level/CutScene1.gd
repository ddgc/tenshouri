extends AudioStreamPlayer2D

var start: AudioStream = preload("res://Assets/Music/night_theme_1_start.wav")
var loop = preload("res://Assets/Music/night_theme_1_loop.wav")
var end = preload("res://Assets/Music/night_theme_1_end.wav")

var changed = false

# Called when the node enters the scene tree for the first time.
func _ready():
	stream = start
	volume_db = -5
	play()

func _on_AudioStreamPlayer2D_finished():
	if not changed:
		changed = true
		stream = loop
		stream.loop_mode = stream.LOOP_FORWARD
		play()
