extends Node2D

signal finished()

onready var dialogue_box = $DialogueBox
onready var tween = $Tween
onready var tween2 = $Tween2
onready var galea = $QueenGalea
onready var galea_sprite = $QueenGalea/QueenGalea

onready var game_scene = get_parent()
onready var level_scene = game_scene.get_node(Levels.getCurrentLevel())
onready var hud = game_scene.get_node("UI/HUD")
onready var camera = game_scene.get_node("Camera2D")
onready var music = $AudioStreamPlayer2D

func _ready():
	level_scene.visible = false
	hud.visible = false
	self.position = camera.position

	yield(get_tree().create_timer(1), "timeout")

	tween.remove_all()
	tween.interpolate_property(galea, "scale", Vector2(0.9, 0.9), Vector2(1, 1), 1, Tween.TRANS_QUAD, Tween.EASE_OUT)
	tween.interpolate_property(galea, "modulate:v", 0, 1.0, 1.5, Tween.TRANS_QUAD, Tween.EASE_OUT)
	tween.start()
	galea.modulate.v = 0
	galea.visible = true
	yield(tween, "tween_all_completed")
	yield(get_tree().create_timer(2), "timeout")

	dialogue_box.start_dialogue("Empress Galea", "Welcome to Erde, outlander.")
	yield(dialogue_box, "dialogue_finished")
	yield(get_tree().create_timer(0.5), "timeout")

	dialogue_box.start_dialogue("Empress Galea", "I am Galea Mosima, Empress of the Empire of Erde.")
	yield(dialogue_box, "dialogue_finished")
	yield(get_tree().create_timer(0.5), "timeout")

	dialogue_box.start_dialogue("Empress Galea", "Erde is the name of this continent that we live in.")
	yield(dialogue_box, "dialogue_finished")
	yield(get_tree().create_timer(0.5), "timeout")

	dialogue_box.start_dialogue("Empress Galea", "A once prosperous and beautiful continent filled with many living things.")
	yield(dialogue_box, "dialogue_finished")
	yield(get_tree().create_timer(0.5), "timeout")

	dialogue_box.start_dialogue("Empress Galea", "But a few years ago, out of nowhere, a distortion of space and time caused gates from other realms to open in all corners of Erde. Through that gate, monsters came and invaded Erde.")
	yield(dialogue_box, "dialogue_finished")
	yield(get_tree().create_timer(0.5), "timeout")

	dialogue_box.start_dialogue("Empress Galea", "Through that gate, monsters came and invaded Erde.")
	yield(dialogue_box, "dialogue_finished")
	yield(get_tree().create_timer(0.5), "timeout")

	dialogue_box.start_dialogue("Empress Galea", "We are trying to hold them as much as we can, but it won’t be long until the whole Erde is covered in darkness.")
	yield(dialogue_box, "dialogue_finished")
	
	yield(get_tree().create_timer(1), "timeout")
	tween.interpolate_property(galea, "position", galea.position, galea.position + Vector2(0,10), 1, Tween.TRANS_QUAD, Tween.EASE_OUT)
	tween.start()
	galea_sprite.play("Sad")
	yield(get_tree().create_timer(3), "timeout")
	galea_sprite.play("Idle")
	tween.interpolate_property(galea, "position", galea.position, galea.position - Vector2(0,10), 1, Tween.TRANS_QUAD, Tween.EASE_OUT)
	tween.start()
	galea_sprite.frame = 35
	yield(get_tree().create_timer(0.5), "timeout")
	galea_sprite.playing = true

	dialogue_box.start_dialogue("Empress Galea", "That is why you are summoned here.")
	yield(dialogue_box, "dialogue_finished")

	dialogue_box.start_dialogue("Empress Galea", "We believe you have the power to bring back Erde to the beautiful continent like it was back then.")
	yield(dialogue_box, "dialogue_finished")

	dialogue_box.start_dialogue("Empress Galea", "So please outlander, lend us your strength to conquer those monsters.")
	yield(dialogue_box, "dialogue_finished")

	finish()


func finish():
	tween2.stop_all()
	tween2.interpolate_property(self, "modulate:v", self.modulate.v, 0, 1, Tween.TRANS_QUAD, Tween.EASE_OUT)
	tween2.interpolate_property(music, "volume_db", music.volume_db, -50, 1)
	tween2.start()
	yield(tween2, "tween_all_completed")

	level_scene.visible = true
	hud.visible = true

	emit_signal("finished")
	queue_free()
