extends Node2D

export var current_wave = 0

var total_wave

var mob_remaining = 0
var mob_deployed = false 

onready var wave_data = MobGlobal.wave_json[name]
onready var ground_route = get_node("GroundRoute")
onready var air_route = get_node("AirRoute")
onready var tower_res: PackedScene = preload("res://Scenes/Tower/Tower.tscn")
onready var music = $AudioStreamPlayer


func _ready():
	var packed_scene = load("res://Scenes/Level/Cutscene " + Levels.current_level + ".tscn")
	var hud = get_parent().get_node("UI/HUD")
	hud.modulate.v = 0
	self.modulate.v = 0
	
	if packed_scene != null:
		var cutscene = packed_scene.instance()
		get_parent().add_child(cutscene)
		set_physics_process(false)
		yield(cutscene, "finished")
		set_physics_process(true)

	var tween = Tween.new()
	add_child(tween)
	tween.interpolate_property(hud, "modulate:v", 0, 1, 1)
	tween.interpolate_property(self, "modulate:v", 0, 1, 1)
	tween.start()
	yield(tween, "tween_all_completed")

	prepare_wave()
	music.play_it()

func _physics_process(_delta):
	check_wave()

# func _process(_delta):
# 	if Input.is_action_just_pressed("LClick"):
# 		spawn_tower("Elf_Tower", get_viewport().get_mouse_position())
# 	if Input.is_action_just_pressed("RClick"):
# 		var space_state = get_world_2d().direct_space_state
# 		for area in space_state.intersect_point(get_viewport().get_mouse_position(), 32, [], 2147483647, true, true):
# 			var parent = area.collider.get_parent()
# 			if parent is Tower:
# 				parent.upgrade()
# 				break

func _on_AreaBenteng_area_entered(_body):
	get_parent().dec_health_by(10)

func prepare_wave():
	MobGlobal.wave_done = false
	total_wave = len(wave_data)
	yield(get_tree().create_timer(1), "timeout")
	wave_start(wave_data[current_wave])

func wave_start(mob_data):
	total_mob(mob_data)
	spawn_mob(mob_data)

func total_mob(mob_data):
	mob_remaining = 0
	for mob in mob_data:
		mob_remaining += mob[1]

func spawn_mob(mob_data):
	get_parent().ui_node.update_wave(current_wave + 1, total_wave)
	for i in range(len(mob_data)):
		var mob_tscn = load("res://Scenes/Mob/" + mob_data[i][0] + ".tscn")
		for j in range(mob_data[i][1]):
			yield(get_tree().create_timer(mob_data[i][2]), "timeout")
			var mob = mob_tscn.instance()
			mob.set_name(mob_data[i][0])

			if (mob_data[i][3] == "Ground") and (ground_route != null):
				ground_route.add_child(mob)
			elif mob_data[i][3] == "Air" and (air_route != null):
				air_route.add_child(mob)
	
	mob_deployed = true
	current_wave += 1

func check_wave():
	var mob_left = 0
	if ground_route != null:
		for child in ground_route.get_children():
			mob_left += 1
	if air_route != null:
		for child in air_route.get_children():
			mob_left += 1

	if mob_left < 1 and mob_deployed:
		mob_deployed = false
		yield(get_tree().create_timer(3), "timeout")
		
		if current_wave < len(wave_data):
			wave_start(wave_data[current_wave])
		else:
			MobGlobal.wave_done = true

