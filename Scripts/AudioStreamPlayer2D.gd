extends AudioStreamPlayer


onready var start = preload("res://Assets/Music/battle_ongoing_start.wav")
onready var loop = preload("res://Assets/Music/battle_ongoing_loop.wav")

func play_it():
	yield(get_tree().create_timer(1), "timeout")
	stream = start
	volume_db = -5
	play()

func _on_AudioStreamPlayer_finished():
	stream = loop
	play()

