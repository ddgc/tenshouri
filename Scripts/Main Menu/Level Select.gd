extends Control

onready var level_1 = $CanvasLayer/ColorRect/ButtonContainer/Level1
onready var level_2 = $CanvasLayer/ColorRect/ButtonContainer/Level2
onready var level_3 = $CanvasLayer/ColorRect/ButtonContainer/Level3
onready var col_rect = $CanvasLayer/ColorRect
onready var tween: Tween = $"Tween"



func _ready():
	GameSave.load_game()
	
	var unlocked1 = Levels.getSpecificLevel('level1')
	if unlocked1:
		level_1.disabled = false
	else:
		level_1.disabled = true

	var unlocked2 = Levels.getSpecificLevel('level2')
	if unlocked2:
		level_2.disabled = false
	else:
		level_2.disabled = true

	var unlocked3 = Levels.getSpecificLevel('level3')
	if unlocked3:
		level_3.disabled = false
	else:
		level_3.disabled = true

func _on_Level_1_pressed():
	Levels.setCurrentLevel("Level 1")
	to_game()

func _on_Level_2_pressed():
	Levels.setCurrentLevel("Level 2")
	to_game()

func _on_Level_3_pressed():
	Levels.setCurrentLevel("Level 3")
	to_game()

func _on_Back_pressed():
	get_tree().change_scene("res://Scenes/Main Menu/MainMenu.tscn")

func to_game():
	tween.interpolate_property(col_rect, "modulate:v", col_rect.modulate.v, 0, 1, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.interpolate_property(self, "modulate:v", col_rect.modulate.v, 0, 1, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.start()
	yield(tween, "tween_all_completed")
	get_tree().change_scene("res://Scenes/Gameplay/GameScene.tscn")
