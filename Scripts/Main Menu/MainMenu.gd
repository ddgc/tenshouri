extends Node2D

func _on_StartGame_pressed():
	get_tree().change_scene("res://Scenes/Main Menu/Level Select.tscn")

func _on_Exit_pressed():
	get_tree().quit()

func _on_Tutorial_pressed():
	get_tree().change_scene("res://Scenes/Tutorial/Tutorial.tscn")

func _on_Credits_pressed():
	get_tree().change_scene("res://Scenes/Gameplay/CreditScene.tscn")