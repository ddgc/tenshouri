extends Node

var levels = {
	'level1': true,
	'level2': false,
	'level3': false,
}

var current_level

func getSpecificLevel(level):
	return levels[level]
	
func unlockSpecificLevel(level):
	levels[level] = true

func getAllLevels():
	return levels
	
func loadLevels(levels_data):
	levels = levels_data
	
func setCurrentLevel(level):
	current_level = level
	
func getCurrentLevel():
	return current_level
