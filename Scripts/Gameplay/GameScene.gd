extends Node2D

export var base_health = 100
export var base_gold = 200

var map_node

var build_mode = false
var build_valid = false
var build_location
var build_type

var tile_position
var cameraloc = 1

var selected_tower
var tower_upgrade_instance
var select_tower_ui
var select_tower_ui_bg

onready var ui_node = get_node("UI")
onready var tower_upgrade = preload("res://Scenes/Tower/TowerUpgrade.tscn")
onready var tower_res: PackedScene = preload("res://Scenes/Tower/Tower.tscn")
onready var camera = get_node("Camera2D")
onready var cameratween =  get_node("Camera2D/CameraTween")

#=========================================#
# Function called when the gameplay ready #
#=========================================#
func _ready():
	var curr_lvl = Levels.getCurrentLevel()
	var lvl = ResourceLoader.load("res://Scenes/Level/"+ curr_lvl +".tscn")
	var lvlinst = lvl.instance()
	get_tree().get_root().get_node("GameScene").add_child(lvlinst)
	map_node = get_node(curr_lvl)
	get_node("UI/HUD/LevelinfoHbox/Levelinfo").text = curr_lvl
	for i in get_tree().get_nodes_in_group("build_buttons"):
		i.connect("pressed", self, "initiate_build_mode", [i.get_name()])
	set_health(base_health)
	set_gold(base_gold)
	if curr_lvl == "Level 1":
		get_node("UI/HUD/viewchange").visible = false
		get_node("UI/HUD/change_view").visible = false
		get_node("UI/HUD/change_bg").visible = false
	
	get_node("UI/HUD/change_view").set_flip_v(true)
	
func _process(_delta):
	if build_mode:
		update_tower_preview()

	if (MobGlobal.wave_done):
		if Levels.getCurrentLevel() == "Level 1":
			Levels.unlockSpecificLevel("level2")
		# elif Levels.getCurrentLevel() == "Level 2":
		# 	Levels.unlockSpecificLevel("level3")
		
		GameSave.save_game()
		get_tree().change_scene("res://Scenes/Main Menu/MainMenu.tscn")
		MobGlobal.wave_done = false

func _unhandled_input(event):
	if event.is_action_released("ui_cancel"):
		if build_mode:
			cancel_build_mode()
		else:
			cancel_select_tower()
			cancel_tower_upgrade()
	if event.is_action_released("ui_accept"):
		if build_mode and verify_and_build():
			cancel_build_mode()
		else:
			cancel_select_tower()
			cancel_tower_upgrade()

func cancel_select_tower():
	if selected_tower and is_instance_valid(selected_tower):
		selected_tower.modulate(Color("ffffff"))
		selected_tower.lvl_label.visible = false
		selected_tower = null
		select_tower_ui.visible = false
		select_tower_ui_bg.visible = false

func cancel_tower_upgrade():
	if tower_upgrade_instance and is_instance_valid(tower_upgrade_instance):
		tower_upgrade_instance.queue_free()
		tower_upgrade_instance = null

func _on_tower_destroy(tower):
	var tilemap = map_node.get_node("TowerExclusion")
	var tile_pos = tilemap.world_to_map(tilemap.to_local(tower.global_position))
	map_node.get_node("TowerExclusion").set_cellv(tile_pos, -1)

func _on_tower_clicked(tower: Tower):
	cancel_select_tower()
	if not build_mode and not is_instance_valid(tower_upgrade_instance):
		selected_tower = tower
		selected_tower.lvl_label.visible = true
		select_tower_ui.visible = true
		select_tower_ui_bg.visible = true
		if tower.tower_name == "Flame_Thrower":
			select_tower_ui.get_node("twname").text = "Flame Thrower"
			select_tower_ui.get_node('tpreview').texture = load("res://Assets/Art/flamethrower_card.png")
		elif tower.tower_name == "Elf_Tower":
			select_tower_ui.get_node("twname").text = "Elf Tower"
			select_tower_ui.get_node('tpreview').texture = load("res://Assets/Art/"+ tower.tower_name + "_card.png")
		else:
			select_tower_ui.get_node("twname").text = tower.tower_name
			select_tower_ui.get_node('tpreview').texture = load("res://Assets/Art/"+ tower.tower_name + "_card.png")
		selected_tower.modulate(Color("ad54ff3c"))

func _on_upgradebtn_pressed():
	if tower_upgrade_instance != null and is_instance_valid(tower_upgrade_instance):
		tower_upgrade_instance.cancel()
	
	tower_upgrade_instance = tower_upgrade.instance()
	tower_upgrade_instance.target_tower = selected_tower
	tower_upgrade_instance.global_position = selected_tower.global_position
	cancel_select_tower()
	add_child(tower_upgrade_instance)

func _on_destroybtn_pressed():
	selected_tower.queue_free()
	select_tower_ui.visible = false
	select_tower_ui_bg.visible = false
	selected_tower = null

#========================#
# Build related function #
#========================#
func initiate_build_mode(towername):
	if !build_mode and towername in TowerGlobal.towr_dict and base_gold >= TowerGlobal.towr_dict[towername].price:
		build_type = towername
		build_mode = true
		ui_node.set_tower_preview(build_type, get_global_mouse_position())

func cancel_build_mode():
	if build_mode:
		build_mode = false
		build_valid = false
		get_node("UI/TowerPreview").queue_free()

func verify_and_build():
	if build_valid:
		if build_type in TowerGlobal.towr_dict:
			if base_gold >= TowerGlobal.towr_dict[build_type].price:
				dec_gold_by(TowerGlobal.towr_dict[build_type].price)
				spawn_tower(build_type, tile_position)
		return true
	else:
		return false

func spawn_tower(tower_name: String, global_pos: Vector2):
	var tilemap = map_node.get_node("TowerExclusion")
	var local_pos = tilemap.to_local(global_pos)
	var map_pos = tilemap.world_to_map(local_pos)
	tilemap.set_cellv(map_pos, 0)

	#Spawn tower entity
	var mid = tilemap.map_to_world(map_pos) + tilemap.cell_size / 2
	var tower = tower_res.instance()
	tower.set_name(tower_name)
	tower.position = to_local(tilemap.to_global(mid))
	tower.tile_pos = map_pos
	add_child(tower)
	tower.connect("clicked", self, "_on_tower_clicked")
	tower.connect("destroyed", self, "_on_tower_destroy")

	select_tower_ui = get_node("UI/HUD/towerselected")
	select_tower_ui_bg = get_node("UI/HUD/towerselectedbg")

func update_tower_preview():
	var mouse_pos = get_global_mouse_position()
	var currrent_tile = map_node.get_node("TowerExclusion").world_to_map(mouse_pos/2)
	var tile_rel = map_node.get_node("TowerExclusion").map_to_world(currrent_tile*2)
	tile_position = map_node.get_node("TowerExclusion").map_to_world(currrent_tile*2)
	if (cameraloc == 2):
		tile_rel.y -= 640

	if map_node.get_node("TowerExclusion").get_cellv(currrent_tile) == -1 and map_node.get_node("Tower").get_cellv(currrent_tile) == -1:
		ui_node.update_tower_preview(tile_rel, "ad54ff3c")
		build_valid = true
		build_location = tile_position
	else:
		ui_node.update_tower_preview(tile_rel, "adff4545")
		build_valid = false

#=========================#
# Health related Function #
#=========================#
func dec_health_by(damage):
	base_health -= damage
	if base_health <= 0:
		get_tree().change_scene("res://Scenes/Main Menu/MainMenu.tscn")
	ui_node.update_health(base_health)

func set_health(new_health):
	ui_node.update_health(new_health)

#+======================#
# Gold related Function #
#=======================#
func dec_gold_by(gold):
	base_gold -= gold
	ui_node.update_gold(base_gold)

func inc_gold_by(gold):
	base_gold += gold
	ui_node.update_gold(base_gold)

func set_gold(new_gold):
	ui_node.update_gold(new_gold)


#===========================#
#        For Testing        #
#===========================#

# func _physics_process(delta):
# 	get_input()


# func get_input():
# 	if Input.is_action_just_pressed("ui_up"):
# 		dec_health_by(5)
# 	if Input.is_action_just_pressed("ui_left"):
# 		inc_gold_by(10)
# 	if Input.is_action_just_pressed("ui_down"):
# 		dec_gold_by(5)


func _on_Main_Menu_pressed():
	pass # Replace with function body.


func _on_viewchange_pressed():
	if (cameraloc == 1):
		cameraloc = 2
		cameratween.interpolate_property(camera, "position", camera.position, Vector2(512, 960), 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		cameratween.start()
		# camera.position = Vector2(512, 960)
	else:
		cameraloc = 1
		cameratween.interpolate_property(camera, "position", camera.position, Vector2(512, 320), 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		cameratween.start()
		# camera.position = Vector2(512, 320)


func _on_change_view_pressed():
	if (cameraloc == 1):
		cameraloc = 2
		cameratween.interpolate_property(camera, "position", camera.position, Vector2(512, 960), 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		cameratween.start()
		get_node("UI/HUD/change_view").set_flip_v(false) 
		# camera.position = Vector2(512, 960)
	else:
		cameraloc = 1
		cameratween.interpolate_property(camera, "position", camera.position, Vector2(512, 320), 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		cameratween.start()
		get_node("UI/HUD/change_view").set_flip_v(true) 
		# camera.position = Vector2(512, 320)
