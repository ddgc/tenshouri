extends CanvasLayer

onready var hpint = get_node("HUD/goldhpinfo/hpcontainer/hpint")
onready var goldint = get_node("HUD/goldhpinfo/goldcontainer/goldint")
onready var waveint = get_node("HUD/LevelinfoHbox/Waveint")

func update_health(health):
	hpint.text = str(health)

func update_gold(gold):
	goldint.text = str(gold)

func update_wave(wave, total_wave):
	waveint.text = str(wave) + "/" + str(total_wave)

func set_tower_preview(build_type, moues_pos):
	var drag_tower = load("res://Scenes/UI/TowerPreview/"+ build_type +"Preview.tscn").instance()
	drag_tower.set_name("DragTower")
	drag_tower.modulate = Color("ad54ff3c")

	var control = Control.new()
	control.add_child(drag_tower, true)
	control.rect_position = moues_pos
	control.set_name("TowerPreview")
	add_child(control, true)
	move_child(get_node("TowerPreview"), 0)	

func update_tower_preview(new_pos, color):
	get_node("TowerPreview").rect_position = new_pos
	if get_node("TowerPreview/DragTower").modulate != Color(color):
		get_node("TowerPreview/DragTower").modulate = Color(color)
