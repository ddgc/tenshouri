extends Node

func save_game():
	var save_game = File.new()
	save_game.open("user://savegame.save", File.WRITE)
	save_game.store_line(to_json(Levels.getAllLevels()))
	save_game.close()

func load_game():
	var save_game = File.new()
	if not save_game.file_exists("user://savegame.save"):
		return
	save_game.open("user://savegame.save", File.READ)
	print(JSON.parse(save_game.get_as_text()).result)
	Levels.loadLevels(JSON.parse(save_game.get_as_text()).result)
	save_game.close()
