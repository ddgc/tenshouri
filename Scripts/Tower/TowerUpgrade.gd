extends Node2D

var target_tower: Tower = null
var blink_map = {}
var tower_arr = [null, null, null, null, null, null, null, null]

var num_selected = 0

onready var timer = $Timer
onready var select_area = $SelectArea

func _ready():
	set_process(true)
	init_sacrificing()

func init_sacrificing():
	if target_tower != null and is_instance_valid(target_tower):
		tower_arr = target_tower.get_surround_tower()
		for i in range(len(tower_arr)):
			var twr = tower_arr[i]
			if twr:
				blink_map[twr] = false
			else:
				get_node("LinkButton" + str(i)).visible = false
		if len(blink_map) == 0:
			queue_free()
		

func _process(_delta):
	var val = abs((timer.time_left / timer.wait_time) - 0.5) * 1.25 + 0.25
	for tower in blink_map.keys():
		if blink_map[tower]:
			tower.modulate = Color(1,0.5,0.5,1)
		else:
			tower.modulate = Color(1,1,1,val)

func queue_free():
	set_process(false)
	for tower in blink_map.keys():
		if tower:
			tower.modulate = Color(1,1,1,1)
	.queue_free()

func _on_LinkButton_pressed():
	var n_destroyed = 0
	for tow in tower_arr:
		if tow and blink_map[tow]:
			n_destroyed += 1
			tow.queue_free()
	if n_destroyed > 0:
		target_tower.upgrade(n_destroyed)
		queue_free()

func _on_LinkButton_Tower_pressed(index:int):
	var tow = tower_arr[index]
	if tow:
		if not blink_map[tow]:
			if num_selected < 3:
				blink_map[tow] = true
				num_selected += 1
		else:
			blink_map[tow] = false
			num_selected -= 1
		if num_selected > 0:
			$UpgradeBg/LinkButton.disabled = false
			$UpgradeBg.modulate.a = 1
		else:
			$UpgradeBg/LinkButton.disabled = true
			$UpgradeBg.modulate.a = 0.25
