extends Node

var towr_dict: Dictionary = {}
var proj_dict: Dictionary = {}
var sacrifice_dict: Dictionary = {}
var sound_dict: Dictionary = {}
var rng := RandomNumberGenerator.new()

func _ready():
	rng.randomize()
	var file = File.new()
	file.open("res://Data/Tower/TowerStats.json", file.READ)
	towr_dict = JSON.parse(file.get_as_text()).result
	file.open("res://Data/Tower/ProjectileStats.json", file.READ)
	proj_dict = JSON.parse(file.get_as_text()).result
	file.open("res://Data/Tower/SacrificeNumber.json", file.READ)
	sacrifice_dict = JSON.parse(file.get_as_text()).result
	file.open("res://Data/Tower/TowerSounds.json", file.READ)
	sound_dict = JSON.parse(file.get_as_text()).result
	file.close()