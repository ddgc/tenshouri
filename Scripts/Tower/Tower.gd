extends Node2D

class_name Tower

signal clicked(instance)
signal destroyed(instance)

enum State {
	IDLE,
	ATTACK
}

var tower_name = "Elf_Tower" setget set_name
var tile_pos = Vector2(0,0)

var atk = 100
var atk_speed = 1
var stg_power = 5
var is_aoe = false
var is_land = true
var is_air = true

var state = State.IDLE
var target_area: Area2D = null
var attack_style = null

var axis_y = Vector2(1,0)

var tower_head: AnimatedSprite = null
var tower_base: Sprite = null

var lvl = 0
var dir = Vector2(0,-1)

onready var area: Area2D = $DetectArea
onready var attack_area: Area2D = $AttackArea
onready var timer_atk: Timer = Timer.new()
onready var upgr_det_area: Area2D = $UpgradeDetect
onready var upgr_src_area: Area2D = $UpgradeSearch

onready var sfx_shoot: AudioStream = null
onready var sfx_hit: AudioStream = null
onready var lvl_label: Label = $Node2D/LvlLabel

func _ready():
	set_process(true)
	
	timer_atk.one_shot = true
	add_child(timer_atk)

	load_stats()

func load_stats():
	var stats = TowerGlobal.towr_dict[tower_name]
	atk = stats["atk"]
	atk_speed = stats["atk_speed"]
	stg_power = stats["stg_power"] 
	is_land = stats["land"]
	is_air = stats["air"]
	area.set_collision_mask_bit(0, is_land)
	area.set_collision_mask_bit(1, is_air)
	attack_area.set_collision_mask_bit(0, is_land)
	attack_area.set_collision_mask_bit(1, is_air)
	set_attack_style(stats["atk_type"])

	if tower_head != null:
		remove_child(tower_head)
		tower_head.queue_free()
	tower_head = load("res://Scenes/Tower/TowerHead" + tower_name + ".tscn").instance()
	add_child(tower_head)

	if tower_base != null:
		remove_child(tower_base)
		tower_base.queue_free()
	tower_base = load("res://Scenes/Tower/TowerBase" + tower_name + ".tscn").instance()
	add_child(tower_base)

	if "sfx_shoot" in TowerGlobal.sound_dict[tower_name]:
		sfx_shoot = load(TowerGlobal.sound_dict[tower_name].sfx_shoot)
		sfx_shoot.loop = false

	if "sfx_hit" in TowerGlobal.sound_dict[tower_name]:
		sfx_hit = load(TowerGlobal.sound_dict[tower_name].sfx_hit)
		sfx_hit.loop = false
	
func _process(_delta):
	match state:
		State.IDLE:
			var areas = area.get_overlapping_areas()
			if len(areas) > 0:
				areas.sort_custom(self, "sort_mob_closest")
				target_area = areas[0]
				state = State.ATTACK
			tower_head.play("Idle")
		State.ATTACK:
			if is_instance_valid(target_area) and area.overlaps_area(target_area):
				if timer_atk.time_left <= 0:
					var mob = target_area.get_parent()
					
					attack(mob)
					if sfx_shoot != null:
						GlobalSound.play(GlobalSound.Type.NON_POSITIONAL, get_node("/root"), sfx_shoot, TowerGlobal.sound_dict[tower_name].shoot_vol, TowerGlobal.rng.randf_range(0.75,1.25), 100.0)
					tower_head.play_anim("Attack", 0)

					timer_atk.start(atk_speed)
				var cur_dir = (target_area.global_position - global_position).normalized()
				tower_head.rotation += dir.angle_to(cur_dir)
				dir = cur_dir
			else:
				target_area = null
				state = State.IDLE


#=======  Inits  =======#

func set_name(t_name: String):
	tower_name = t_name

func set_attack_style(atk_style: String):
	if attack_style != null:
		attack_style.queue_free()

	var res = load("res://Scripts/Tower/Attack" + atk_style + ".gd")
	if res != null:
		attack_style = res.new()
		attack_style.set_tower(self)

#=======  Behavior  =======#

func attack(mob: Mob):
	if attack_style != null and mob != null and is_instance_valid(mob) and mob.hp > 0:
		attack_style.attack(mob)

func get_surround_tower() -> Array:
	var towers = [null, null, null, null, null, null, null, null]
	var index_zero = tile_pos - Vector2(1,1)
	for tower in upgr_src_area.get_overlapping_areas():
		var twr = tower.get_parent()
		if "tower_name" in twr and twr.tower_name == tower_name and twr != self:
			var pos = twr.tile_pos - index_zero
			var index = pos.x + pos.y * 3
			if index > 3:
				index -= 1
			if index < 8:
				towers[index] = twr
	return towers

# func upgradeAuto():
# 	var towers = []
# 	for tower in upgr_src_area.get_overlapping_areas():
# 		var twr = tower.get_parent()
# 		if "tower_name" in twr and twr.tower_name == tower_name and tower != upgr_det_area:
# 			towers.append(twr)
	
# 	var sacrifice_num = min(len(towers), 3)
# 	if sacrifice_num == 0:
# 		return

# 	upgrade(sacrifice_num)

# 	var tower_tilemap: TileMap = get_parent().get_node("Tower")
# 	for i in range(sacrifice_num):
# 		var towr = towers[i]
# 		var pos = tower_tilemap.world_to_map(towr.position)
# 		tower_tilemap.set_cellv(pos, -1)
# 		towr.queue_free()

func upgrade(sacrifice_num: int):
	atk += TowerGlobal.sacrifice_dict[str(sacrifice_num)].atk
	atk_speed += TowerGlobal.sacrifice_dict[str(sacrifice_num)].aspd
	atk_speed = max(atk_speed,0.25)
	lvl += sacrifice_num
	lvl_label.text = "Lvl. " + str(lvl)

#=======  Helper  =======#

func sort_mob_closest(a: Area2D, b: Area2D):
	return a.get_parent().offset < b.get_parent().offset

func modulate(col):
	tower_base.modulate = col
	tower_head.modulate = col

func _on_TowerClick_pressed():
	emit_signal("clicked", self)

func queue_free():
	emit_signal("destroyed", self)
	if is_instance_valid(self):
		.queue_free()

func get_lvl() -> int:
	return lvl
