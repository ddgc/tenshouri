extends Node2D

class_name Projectile

enum State {
	START,
	CHASE,
	WANDER,
	HIT,
	TIMEOUT
}

export (int) var despawn_time = 0.5

var target: Mob setget set_target
var tower: Tower setget set_tower_source
var tower_name = ""
var atk = 0
var stg_power = 0
var sfx_hit = null
var speed := 800
var axis_y = Vector2(0,1)
var anim_sprite: AnimatedSprite = null
var state = State.START
var dir = Vector2(0,1)

var ai_func = "projectile_ai"

var detected_mob = null

onready var detect_area: Area2D = $DetectArea
onready var splash_area: Area2D = $SplashArea
onready var splash_shape: CollisionShape2D = $SplashArea/CollisionShape2D
onready var timer_despawn: Timer = $DespawnTimer

func _ready():
	set_process(true)
	var as_res = load("res://Scenes/Tower/Proj" + tower.tower_name + ".tscn")
	speed = TowerGlobal.proj_dict[tower.tower_name].speed
	tower_name = tower.tower_name
	sfx_hit = tower.sfx_hit
	atk = tower.atk
	stg_power = tower.stg_power

	var cur_dir = (target.global_position - self.global_position).normalized()
	self.rotation += dir.angle_to(cur_dir)
	dir = cur_dir
	
	if as_res != null:
		anim_sprite = as_res.instance()
		add_child(anim_sprite)

		detect_area.monitoring = true
		detect_area.set_collision_mask_bit(0, tower.is_land)
		detect_area.set_collision_mask_bit(1, tower.is_air)

		splash_area.monitoring = true
		splash_area.set_collision_mask_bit(0, tower.is_land)
		splash_area.set_collision_mask_bit(1, tower.is_air)

		splash_shape.shape.radius = TowerGlobal.towr_dict[tower.tower_name].splash
	else:
		queue_free()

func _process(delta):
	call(ai_func, delta)

# func aoe_ai(delta):
# 	print(target)
# 	match state:
# 		State.START:
# 			state = State.CHASE
# 			timer_despawn.start()
# 		State.CHASE:
# 			if is_instance_valid(target) and target.hp > 0:
# 				global_position = global_position.move_toward(target.global_position, speed * delta)
# 				var cur_dir = (target.global_position - self.global_position).normalized()
# 				self.rotation += dir.angle_to(cur_dir)
# 				anim_sprite.play("Fly")
# 				dir = cur_dir
# 			else:
# 				state = State.WANDER
# 				dir = Vector2(0,1).rotated(self.rotation).normalized()
# 				timer_despawn.start()
# 				detect_area.monitoring = true
# 		State.WANDER:
# 			global_position += dir * delta * speed
# 		State.HIT:
# 			if sfx_hit != null:
# 				GlobalSound.play(GlobalSound.Type.NON_POSITIONAL, get_node("/root"), sfx_hit, -10)
# 			anim_sprite.play("Hit")

# 			queue_free()
# 		State.TIMEOUT:
# 			state = State.HIT

func projectile_ai(delta):
	match state:
		State.START:
			timer_despawn.start()
			state = State.CHASE
		State.CHASE:
			if is_instance_valid(target) and target.hp > 0:
				global_position = global_position.move_toward(target.global_position, speed * delta)
				var cur_dir = (target.global_position - self.global_position).normalized()
				self.rotation += dir.angle_to(cur_dir)
				anim_sprite.play("Fly")
				dir = cur_dir
			else:
				state = State.WANDER
				dir = Vector2(0,1).rotated(self.rotation).normalized()
				detect_area.monitoring = true
		State.WANDER:
			global_position += dir * delta * speed
		State.HIT:
			if sfx_hit != null:
				GlobalSound.play(GlobalSound.Type.NON_POSITIONAL, get_node("/root"), sfx_hit, TowerGlobal.sound_dict[tower_name].hit_vol, TowerGlobal.rng.randf_range(0.75,1.25), 100.0)

			for mob_area in splash_area.get_overlapping_areas():
				var mob = mob_area.get_parent()
				if mob is Mob and mob != detected_mob:
					mob.hit(atk, stg_power)

			if "DOT" in TowerGlobal.proj_dict[tower_name]:
				var dot = load("res://Scenes/Tower/DOT.tscn").instance()
				dot.position = position
				get_parent().add_child(dot)

			anim_sprite.play("Hit")
			queue_free()
		State.TIMEOUT:
			state = State.HIT

func set_target(tgt: Mob):
	target = tgt

func set_tower_source(twr: Tower):
	tower = twr

func _on_DetectArea_area_entered(area: Area2D):
	detected_mob = area.get_parent()
	if is_instance_valid(detected_mob) and detected_mob is Mob:
		detected_mob.hit(atk, stg_power)
	state = State.HIT

func _on_DespawnTimer_timeout():
	state = State.TIMEOUT
