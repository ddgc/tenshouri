extends Node2D

var goblin
var current_wave = 0
var mob_left = 0
var mob_tscn

onready var goblin_tscn = load("res://Scenes/Mob/Goblin.tscn")
onready var wyvern_tscn = load("res://Scenes/Mob/Wyvern.tscn")

onready var tower_tile_map: TileMap = $Tower
onready var tower_tile_set: TileSet = tower_tile_map.tile_set
onready var tower_res: PackedScene = preload("res://Scenes/Tower/Tower.tscn")

func _ready():
	for cell in tower_tile_map.get_used_cells():
		var tower_name = tower_tile_set.tile_get_name(tower_tile_map.get_cellv(cell))
		var spawn_pos = tower_tile_map.map_to_world(cell)
		spawn_tower(tower_name, spawn_pos)

	$WaveTimer.start()

func _process(_delta):
	if Input.is_action_just_pressed("LClick"):
		spawn_tower("Elf_Tower", get_viewport().get_mouse_position())
	if Input.is_action_just_pressed("RClick"):
		var space_state = get_world_2d().direct_space_state
		for area in space_state.intersect_point(get_viewport().get_mouse_position(), 32, [], 2147483647, true, true):
			var parent = area.collider.get_parent()
			if parent is Tower:
				parent.upgrade()
				break


func _on_WaveTimer_timeout():
	$MobTimer.start()

func _on_MobTimer_timeout():
	if MobGlobal.mob_wave[current_wave] > 0:
		# goblin = goblin_tscn.instance()
		goblin = wyvern_tscn.instance()
		goblin.set_name("Wyvern")
		# $Path2D.add_child(goblin)
		$Path2D2.add_child(goblin)
		MobGlobal.mob_wave[current_wave] -= 1
	
	mob_left = 0
	for child in $Path2D2.get_children():
		mob_left += 1
	
	if mob_left < 1:
		$MobTimer.stop()
		current_wave += 1
		if current_wave < len(MobGlobal.mob_wave):
			$WaveTimer.start()

"""
Spawns tower based on registered name. 
The TileSet must contain tile with the name input or else it will do nothing. 
It is recommended to use already registered names.

tower_name: Registered tower name
pos: Global position
"""
func spawn_tower(tower_name: String, global_pos: Vector2):
	var tile_id = tower_tile_set.find_tile_by_name(tower_name)
	if tile_id != -1 and TowerGlobal.towr_dict.has(tower_name):
		#Place tower tile
		var local_pos = tower_tile_map.to_local(global_pos)
		var map_pos = tower_tile_map.world_to_map(local_pos)
		tower_tile_map.set_cellv(map_pos, tile_id)
		
		#Spawn tower entity
		var mid = tower_tile_map.map_to_world(map_pos) + tower_tile_map.cell_size / 2
		var tower = tower_res.instance()
		tower.set_name(tower_name)
		tower.position = mid
		add_child(tower)
