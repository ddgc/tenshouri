extends AnimatedSprite

onready var tower = get_parent()
onready var timer: Timer = tower.timer_atk

func play_anim(anim: String, frameI: int):
	if frames.has_animation(anim):
		play(anim)
		frame = frameI
		var max_frames = frames.get_frame_count(anim)
		frames.set_animation_speed(anim, max_frames/tower.atk_speed)
