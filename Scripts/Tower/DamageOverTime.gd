extends Node2D

export var die_timer = 0.4
export var born_timer = 0.1
onready var hit_area: Area2D = $Area2D
var atk = 0.2
var stg_power = 0
var born = true
var dying = false
var die_time = die_timer
var born_time = born_timer

func _ready():
	set_process(true)
	modulate.a = 0

func _process(_delta):
	if born:
		born_time -= _delta
		modulate.a = 1.0 - (born_time/born_timer)
		if modulate.a >= 1.0:
			born = false
	for mob_area in hit_area.get_overlapping_areas():
		var mob = mob_area.get_parent()
		if mob is Mob and is_instance_valid(mob):
			mob.hit_no_def(atk, stg_power)
	if dying:
		die_time -= _delta
		modulate.a = die_time/die_timer
		if die_time <= 0:
			queue_free()

func _on_Timer_timeout():
	dying = true
