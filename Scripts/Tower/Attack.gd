class_name Attack

var atk := 100
var stg_power := 5
var attack_area: Area2D = null
var tower: Tower = null

func set_tower(towr: Tower):
	tower = towr
	atk = tower.atk
	stg_power = tower.stg_power
	attack_area = tower.attack_area

func attack(_mob: PathFollow2D):
	pass
