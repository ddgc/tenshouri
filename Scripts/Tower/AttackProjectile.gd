extends Attack

class_name AttackProjectile

var projectile_res: Resource = preload("res://Scenes/Tower/Projectile.tscn")

func attack(_mob: PathFollow2D):
	if is_instance_valid(_mob):
		var proj = projectile_res.instance()
		proj.set_target(_mob)
		proj.set_tower_source(self.tower)
		proj.global_position = self.tower.global_position
		self.tower.get_tree().get_root().add_child(proj)
