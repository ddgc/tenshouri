extends Node

var current_wave = 0
var mob_deployed = false

onready var wave_data = MobGlobal.wave_json

func _ready():
	$WaveTimer.start()
	# prepare_wave()

func prepare_wave():
	yield(get_tree().create_timer(1), "timeout")
	wave_start(wave_data[current_wave])

func wave_start(mob_data):
	spawn_mob(mob_data)

func spawn_mob(mob_data):
	for i in range(1, len(mob_data)):
		var mob_tscn = load("res://Scenes/Mob/" + mob_data[i][0] + ".tscn")

		for j in range(0, mob_data[i][1]):
			yield(get_tree().create_timer(mob_data[i][2]), "timeout")
			var mob = mob_tscn.instance()
			mob.set_name(mob_data[i][0])
			$Path2D.add_child(mob)
	
	mob_deployed = true
	current_wave += 1

func check_wave():
	var mob_left = 0
	for child in $Path2D.get_children():
		mob_left += 1

	if mob_left < 1 and mob_deployed:
		mob_deployed = false
		yield(get_tree().create_timer(3), "timeout")
		wave_start(wave_data[current_wave])

# func _on_WaveTimer_timeout():
# 	$MobTimer.start()

# func _on_MobTimer_timeout():
# 	if MobGlobal.mob_wave[current_wave] > 0:
# 		goblin = goblin_tscn.instance()
# 		goblin.set_name("Goblin")
# 		$Path2D.add_child(goblin)
# 		MobGlobal.mob_wave[current_wave] -= 1
	
# 	var mob_left = 0
# 	for child in $Path2D.get_children():
# 		mob_left += 1
	
# 	if mob_left < 1:
# 		$MobTimer.stop()
# 		current_wave += 1
# 		if current_wave < len(MobGlobal.mob_wave):
# 			$WaveTimer.start()
