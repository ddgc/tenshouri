extends Node

var file = File.new()
var mob_json: Dictionary = {}
var wave_json: Dictionary = {}
var wave_done

func _ready():
	file.open("res://Data/Mob/MobList.json", file.READ)
	mob_json = JSON.parse(file.get_as_text()).result

	file.open("res://Data/Mob/Wave.json", file.READ)
	wave_json = JSON.parse(file.get_as_text()).result