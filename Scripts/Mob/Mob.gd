extends PathFollow2D

class_name Mob

var mob_name = "Mob"
var hp: float = 0.0
var speed = 0
var def = 0
var stg_res = 0
var gold_drop = 0
var stagger = 0
var isDead = false
var isHealthBarFree = false
var sfx_vol = 0

var dmg_time = 0

onready var health_bar = get_node("HealthBar")
onready var sfx_dead: AudioStream = null

func _ready():
	load_statmob()
	initiate_healthbar()

func load_statmob():
	var mob_stats = MobGlobal.mob_json[mob_name]
	hp = mob_stats["hp"]
	speed = mob_stats["speed"]
	def = mob_stats["def"]
	stg_res = mob_stats["stg_res"]
	gold_drop = mob_stats["gold_drop"]

	if "sfx_dead" in mob_stats:
		sfx_dead = load(mob_stats.sfx_dead[0])
		sfx_vol = mob_stats.sfx_dead[1]
		if sfx_dead != null:
			sfx_dead.loop = false

func initiate_healthbar():
	health_bar.max_value = hp
	health_bar.value = hp
	health_bar.set_as_toplevel(true)

func _physics_process(delta):
	if !isDead:
		move(delta)

	check_dead()

	dmg_time -= delta
	dmg_time = max(dmg_time, 0)
	if dmg_time > 0:
		modulate = Color("ff6868")
	else:
		modulate = Color("ffffff")

func move(delta):
	offset += speed * delta

	if mob_name == "Wyvern":
		health_bar.set_position(global_position - Vector2(health_bar.rect_size.x/2, 45))
	elif mob_name == "Dragon":
		health_bar.set_position(global_position - Vector2(health_bar.rect_size.x/2, 60))
	else:	
		health_bar.set_position(global_position - Vector2(health_bar.rect_size.x/2, 15))

	$AnimatedSprite.play("Walk")
	if unit_offset >= 1:
		queue_free()

func check_dead():
	if hp <= 0 and !isDead:
		isDead = true

		get_parent().get_parent().get_parent().inc_gold_by(gold_drop)

		if sfx_dead != null:
			GlobalSound.play(GlobalSound.Type.NON_POSITIONAL, get_node("/root"), sfx_dead, sfx_vol)

		$AnimatedSprite.play("Die")

	if $AnimatedSprite.animation == "Die":
		if ($AnimatedSprite.frame == $AnimatedSprite.frames.get_frame_count("Die") / 2) and !isHealthBarFree:
			get_node("HealthBar").queue_free()
			isHealthBarFree = true

		if $AnimatedSprite.frame == $AnimatedSprite.frames.get_frame_count("Die") - 1:
			queue_free()

func set_name(new_name):
	mob_name = new_name

func hit_no_def(dmg, stg_power):
	if !isDead:
		modulate = Color("ff6868")
		hp -= dmg
		health_bar.value = hp
		stagger = max((stg_power - stg_res), 0)
		offset -= stagger
		health_bar.set_position(global_position - Vector2(health_bar.rect_size.x/2, 15))
		dmg_time = 0.2

func hit(dmg, stg_power):
	if !isDead:
		modulate = Color("ff6868")
		dmg = (dmg - def) if (dmg - def) > 0 else 0
		hp -= dmg
		health_bar.value = hp
		stagger = max((stg_power - stg_res), 0)
		offset -= stagger
		health_bar.set_position(global_position - Vector2(health_bar.rect_size.x/2, 15))
		dmg_time = 0.2
